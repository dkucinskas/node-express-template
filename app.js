'use strict';

module.exports = function (app) {

    var express = require('express');
    var path = require('path');
    var engine = require('ejs-locals');

    app.set('views', __dirname + '/views');
    app.engine('ejs', engine);
    app.set('view engine', 'ejs');
    app.use(express.logger('dev'));
    app.use(express.cookieParser());
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(express.favicon(__dirname + '/public/images/favicon.ico'));

    if ('development' === app.get('env')) {
        app.use(express.errorHandler());
    }

    require('./routes').call(null, app);
    require('./di').call(null, app);

};