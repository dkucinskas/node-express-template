/*jslint regexp: true, nomen: true, sloppy: true */
require.config({
    nodeRequire: require,
    baseUrl: '/scripts/',
    paths: {
        jquery: [
            '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min',
            'libs/jquery2.0.3'
        ],
        modal: 'plugins/jquery.modal',
        validation: 'plugins/jquery.validate'
    },
    shim: {
        modal: {
            deps: ['jquery']
        },
        validation: {
            deps: ['jquery']
        }
    }
});