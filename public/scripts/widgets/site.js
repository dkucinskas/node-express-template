/**
 * Created with JetBrains WebStorm.
 * User: PaulG
 * Date: 8/18/13
 * Time: 3:39 PM
 * To change this template use File | Settings | File Templates.
 */

define(['jquery', 'modal', 'validation'], function ($, modal, validation) {
    'use strict';

    var module = {};

    module.modal = function () {
        $(document).on('click', 'a[data-modal]', function (e) {
            e.preventDefault();

            $(this).openModal({
                onLoad: function () {
                    var width = $(this.container).width(),
                        height = $(this.container).height();
                    $(this.container).css('margin-left', -(width / 2).toString() + 'px');
                    $(this.container).css('margin-top', -(height / 2).toString() + 'px');
                }
            });
        });
    };

    module.validation = function () {
        $('form').each(function () {
            $(this).validate({
                errorElement: 'span',
                errorClass: 'validation-error',
                errorPlacement: function (error, element) {
                    $('<span class="field-validation-error"></span>').append(error).insertAfter(element);
                },
                success: function (label) {
                    label.parent().remove();
                }
            });
        });
    };
    module.init = function () {
        module.modal();
        module.validation();
    };
    return module;
});
