"use strict";

var pg = require('pg'),
	sql = require('sql'),
	maps = require('../maps');

function PostRepository (dbUrl) {
	this.dbUrl = dbUrl;
}

PostRepository.prototype.pg = null;

PostRepository.prototype.findAll = function (callback) {
	var query = maps.post
		.select(maps.post.id, maps.post.title, maps.post.body, maps.post.created_at)
		.from(maps.post)
		.order(maps.post.created_at.descending)
		.toQuery(),
		data = [];
	
	pg.connect(this.dbUrl, function (err, client) {
		if (err) {
			console.log("Error while connecting to PG." + err);
		}
		
		client.on('drain', function() {
			client.end.bind(client);
			callback(null, data);
		});
	
		var result = client.query(query);
		
		result.on('error', function (err) {
			console.log(err);
		});
		
		result.on('row', function (row) {
			data.push(row);
			//console.log(JSON.stringify(row));
		});
	});
};

PostRepository.prototype.findById = function (id, callback) {
	var query = maps.post
		.select(maps.post.id, maps.post.title, maps.post.body, maps.post.created_at)
		.from(maps.post)
		.where(maps.post.id.equals(id))
		.toQuery(),
		post = null;
	
	pg.connect(this.dbUrl, function (err, client) {
		if (err) {
			console.log("Error while connecting to PG." + err);
		}
		
		client.on('drain', function() {
			client.end.bind(client);
			callback(null, post);
		});
	
		var result = client.query(query);
		
		result.on('error', function (err) {
			console.log(err);
		});
		
		result.on('row', function (row) {
			post = row;
		});
	});
};

PostRepository.prototype.save = function (posts, callback) {
	
	var query = maps.post.insert(posts).toQuery();
	
	pg.connect(this.dbUrl, function (err, client) {
		if (err) {
			console.log("Error while connecting to PG." + err);
		}
		
		client.on('drain', function() {
			client.end.bind(client);
			callback(null, posts);
		});
	
		var result = client.query(query);
		
		result.on('error', function (err) {
			console.log(err);
		});
		
		result.on('row', function (row) {
		});
	});
};

PostRepository.prototype.remove = function (id, callback) {
	var query = maps.post.delete()
				.where(maps.post.id.equals(id))
				.toQuery();
	
	pg.connect(this.dbUrl, function (err, client) {
		if (err) {
			console.log("Error while connecting to PG." + err);
		}
		
		client.on('drain', function() {
			client.end.bind(client);
			callback(null, true);
		});
	
		var result = client.query(query);
		
		result.on('error', function (err) {
			console.log(err);
			callback(null, false);
		});
		
		result.on('row', function (row) {
		});
	});
};

PostRepository.prototype.update = function (post, callback) {
	var query = maps.post.update(post)
				.where(maps.post.id.equals(post.id))
				.toQuery();
	
	pg.connect(this.dbUrl, function (err, client) {
		if (err) {
			console.log("Error while connecting to PG." + err);
		}
		
		client.on('drain', function() {
			client.end.bind(client);
			callback(null, post);
		});
		
		var result = client.query(query);
		
		result.on('error', function (err) {
			console.log(err);
		});
		
		result.on('row', function (row) {
		});
	});
};

exports.PostRepository = PostRepository;
