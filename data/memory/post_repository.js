"use strict";

var postCount = 1;

function PostRepository(dbUrl) {
    this.dbUrl = dbUrl;
}

PostRepository.prototype.dummyData = [];

PostRepository.prototype.findAll = function (callback) {
	var posts = this.dummyData.slice();
	
	posts.sort(function (a, b) {
		if (a.created_at > b.created_at) {
			return -1;
		}
		
		if (a.created_at < b.created_at) {
			return 1;
		}
		
		return 0;
	});
	
    callback(null, posts);
};

PostRepository.prototype.findById = function (id, callback) {
    var result = null,
        i;

    for (i = 0; i < this.dummyData.length; i += 1) {
        if (this.dummyData[i].id === id) {
            result = this.dummyData[i];
            break;
        }
    }

    callback(null, result);
};

PostRepository.prototype.save = function (posts, callback) {
    var post = null,
        i,
        j;

    if (typeof (posts.length) === 'undefined') {
        posts = [posts];
    }

    for (i = 0; i < posts.length; i += 1) {
        post = posts[i];
        post.id = postCount += 1;
        post.created_at = posts[i].created_at || new Date();

        if (post.comments === undefined) {
            post.comments = [];
        }

        for (j = 0; j < post.comments.length; j += 1) {
            post.comments[j].created_at = new Date();
        }

        this.dummyData[this.dummyData.length] = post;
    }

    callback(null, posts);
};

PostRepository.prototype.remove = function (id, callback) {
    var result = null,
        i;

    for (i = 0; i < this.dummyData.length; i += 1) {
        if (this.dummyData[i].id === id) {
            this.dummyData.splice(i, 1);
            break;
        }
    }

    callback(null, true);
};

PostRepository.prototype.update = function (post, callback) {
    var i;

    for (i = 0; i < this.dummyData.length; i += 1) {

        if (this.dummyData[i].id === post.id) {
            console.log('post found!');

            this.dummyData[i].title = post.title;
            this.dummyData[i].body = post.body;

            break;
        }
    }

    callback(null, true);
};

/* Lets bootstrap with dummy data */
new PostRepository().save([
	{ title: 'Post one', body: 'Body one', created_at: new Date(2013, 8, 1), comments: [
		{ author: 'Bob', comment: 'I love it'},
		{ author: 'Dave', comment: 'This is rubbish!' }
	]},
	{ title: 'Post two', body: 'Body two', created_at: new Date(2013, 8, 2) },
	{ title: 'Post three', body: 'Body three', created_at: new Date(2013, 8, 3)  },
	{ title: 'Post four', body: 'Body four', created_at: new Date(2013, 8, 4)  },
	{ title: 'Post five', body: 'Body five', created_at: new Date(2013, 8, 5)  },
	{ title: 'Post six', body: 'Body six', created_at: new Date(2013, 8, 6)  },
	{ title: 'Post seven', body: 'Body seven', created_at: new Date(2013, 8, 7)  },
	{ title: 'Post eight', body: 'Body eight', created_at: new Date(2013, 8, 8)  },
	{ title: 'Post nine', body: 'Body nine', created_at: new Date(2013, 8, 9)  },
	{ title: 'Post ten', body: 'Body ten', created_at: new Date(2013, 8, 10)  },
	{ title: 'Post eleven', body: 'Body eleven', created_at: new Date(2013, 8, 11)  },
	{ title: 'Post twelve', body: 'Body twelve', created_at: new Date(2013, 8, 12)  },
	{ title: 'Post thirteen', body: 'Body thirteen', created_at: new Date(2013, 8, 13)  }
], function (error, posts) {});

exports.PostRepository = PostRepository;