'use strict';

module.exports = function (app) {

    var site = require('./routes/site');
    app.get('/', site.index);

    var blog = require('./routes/blog');
    app.get('/blog/:id(\\d+)', blog.view);
    app.get('/blog/create', blog.create_form);
    app.post('/blog/create', blog.create);
    app.get('/blog/delete/:id(\\d+)', blog.delete);
    app.get('/blog/edit/:id(\\d+)', blog.edit_form);
    app.post('/blog/edit', blog.edit);

    var partials = require('./routes/partials');
    app.get('/partials/modalcontent', partials.modalcontent);

    var admin = require('./routes/admin');
    app.get('/admin', admin.index);

};