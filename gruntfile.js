'use strict';

module.exports = function (grunt) {

    var path = require('path');

    grunt.initConfig({
        jshint: {
            options: {
                indent: 4,
                curly: true,
                strict: true,
                maxlen: 120,
                trailing: true,
                smarttabs: false
            },
            node: {
                options: {
                    node: true,
                },
                files: {
                    src: [
                        './*.js',
                        './tasks/**/*.js',
                        './routes/**/*.js',
                        './data/**/*.js'
                    ]
                }
            },
            'node-tests': {
                options: {
                    node: true,
                    globals: {
                        describe: true,
                        expect: true,
                        it: true
                    }
                },
                files: {
                    src: [
                        './spec/**/*.js'
                    ]
                }
            },
            'browser-tests': {
                options: {
                    browser: true,
                    strict: false,
                    globals: {
                        describe: true,
                        expect: true,
                        it: true
                    }
                },
                files: {
                    src: [
                        './public/spec/**/*.js'
                    ]
                }
            }
        },
        server: {
            dev: {
                options: {
                    hostname: 'localhost',
                    port: 9000,
                    keepalive: true
                }
            }
        },
        'jasmine-node': {
            options: {
                matchall: true,
                color: true,
                verbose: true
            },
            run: {
                spec: 'spec'
            },
            executable: path.join(__dirname, './node_modules/.bin/jasmine-node')
        },
        karma: {
            options: {
                frameworks: [ 'jasmine' ],
                files: [
                    './public/spec/spec.js'
                ],
                exclude: [],
                port: 9876,
                colors: false,
                autoWatch: true,
                browsers: [ 'Firefox', 'Chrome' ],
                captureTimeout: 20000,
                singleRun: false
            },
            dev: {
                reporters: [ 'progress' ]
            }
        },
        compass: {
            dist: {
                options: {
                    config: 'config.rb'
                    /*httpPath: '/',
                    sassDir: 'sass',
                    cssDir: 'public/styles',
                    imagesDir: 'sass/images',
                    generatedImagesDir: 'public/styles/images',
                    //spriteEngine: 'chunky_png',
                    outputStyle: 'expanded',
                    lineComments: false*/
                }
            }
        },
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['compass']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-jasmine-node');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadTasks('tasks');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('start', [
        'jshint:node',
        'compass',
        'server:dev'
    ]);

    grunt.registerTask('default', [
        'jshint:node',
        'jshint:node-tests',
        'jshint:browser-tests'
    ]);

    grunt.registerTask('test', [
        'jshint:node',
        'jshint:node-tests',
        'jshint:browser-tests',
        'jasmine-node',
        'karma:dev'
    ]);

};
