"use strict";

exports.index = function (req, res) {
    var di = req.app.get('di');

    di.invoke(function (postProvider) {
        postProvider.findAll(function (err, posts) {
            res.render('index', {
                title: 'Posts view',
                posts: posts
            });
        });
    });
};