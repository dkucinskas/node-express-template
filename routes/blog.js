"use strict";

var Post = require('../data/post');

exports.index = function (req, res) {
	var di = req.app.get('di');
	
	di.invoke(function(postProvider) {
		postProvider.findAll(function (err, posts) {
			res.render('index', {
				title: 'Posts view',
				posts: posts
			});
		});
	});
};

exports.create_form = function (req, res) {
	res.render('blog/create', {
		title: 'New post'
	});
};

exports.create = function (req, res) {
	var di = req.app.get('di');
	
	di.invoke(function(postProvider) {
		var post = new Post.Post(req.body);
		delete post.id;
		
		postProvider.save(post, function (err, docs) {
			res.redirect('/');
		});
	});
};

exports.view = function (req, res) {
	var id = parseInt(req.param('id'), 10),
		di = req.app.get('di');
	
	if (id) {
		di.invoke(function(postProvider) {
			postProvider.findById(id, function (error, post) {
				if (post) {
					res.render('blog/view', {
						title: post.title,
						post: post
					});
				} else {
					console.log("Post not found id: " + id);
					res.status(404).render('../error404', {
						title : '404 - Not Found'
					});
				}
			});
		});
	} else {
		console.log("/blog/:id(\\d+) -> Id: (" + id + ") is not a number.");
	}
};

exports.delete = function (req, res) {
	var id = parseInt(req.param('id'), 10),
		di = req.app.get('di');
	
	if (id) {
		di.invoke(function(postProvider) {
			postProvider.remove(id, function (error, success) {
				if (success) {
                    res.redirect('/');
                } else {
					console.log("Post not found id: " + id);
					res.status(404).render('../error404', {
						title : '404 - Not Found'
					});
				}
			});
		});
	} else {
		console.log("/blog/delete/:id(\\d+) -> Id: (" + id + ") is not a number.");
	}
};

exports.edit_form = function (req, res) {
	var id = parseInt(req.param('id'), 10),
		di = req.app.get('di');
	
	if (id) {
		di.invoke(function(postProvider) {
			postProvider.findById(id, function (error, post) {
				if (post) {
					res.render('blog/edit', {
						title: post.title,
						post: post
					});
				} else {
					console.log("Post not found id: " + id);
					res.status(404).render('../error404', {
						title : '404 - Not Found'
					});
				}
			});
		});
	} else {
		console.log("/blog/edit/:id(\\d+) -> Id: (" + id + ") is not a number.");
	}
};

exports.edit = function (req, res) {
	var di = req.app.get('di');
	
	di.invoke(function(postProvider) {
		var post = new Post.Post(req.body);
		
		postProvider.update(post, function (err, docs) {
			res.redirect('/');
		});
	});
};
