'use strict';

module.exports = function (grunt) {

	var http = require('http');
	var express = require('express');

	grunt.registerMultiTask('server', 'Starting a web server.', function() {
		var done = this.async();
		var options = this.options({
			hostname: 'localhost',
			port: 8080,
			keepalive: false
		});
		var app = express();
		require('../app.js').call(null, app);
		var server =http.createServer(app)
			.listen(options.port, options.hostname)
			.on('listening', function () {
				var address = server.address(),
					message = 'Web server has been started on ';
				grunt.log.writeln(message + (address.host || 'localhost') + ':' + address.port + '.');
				if (!options.keepalive) {
					done();
				}
			})
			.on('error', function(err) {
				if (err.code === 'EADDRINUSE') {
					grunt.fatal('Port ' + options.port + ' is already in use by another process.');
				} else {
					grunt.fatal(err);
				}
			});
	});
};