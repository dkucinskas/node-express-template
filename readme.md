# Standard Web Project Template

This standard web project template is based on **Node.js**. The main purpose of this project is to demonstrate how web application can be created using **Node.js**.

## Getting started

To run standard web project template on windows, prerequisites have to be installed:

* Node.js 0.10.* or newer
* Python 2.7.*
* Ruby 1.8.*
* Sass 3.2.*
* Compass 0.12.*

For all Windows users we highly recommend ["Ruby Installer for Windows"](http://rubyinstaller.org/downloads/)
After you installed **Ruby** you should install *'compass'* framework, *'compass'* will install all necessary *'sass'* files.

To install compass from command line type:

    gem install compass

**Node.js** native add'on build tool *'node-gyp'* has to be installed globally:

    npm install -g node-gyp

In order to build project, grunt's command line interface should be installed globally.

    npm install -g grunt-cli

To install application dependencies, command *'npm install'* has to been executed in working directory:

    npm install

To start web server, such command has to be typed:

    grunt start

## FAQ

### Q: I can't install any of the modules on windows that require compilation?

Visual C++ Redistributable for Visual Studio 2010 or newer has to be installed. Command *'npm install'* with aditional parameters can be executed:

    npm install --msvs_version=2012

### Q: I can't install any of the modules on Mac OS X that require compilation?

You might need to install ["Xcode"](https://itunes.apple.com/en/app/xcode/id497799835?mt=12) from AppStore, also in some cases you need to install **Xcode** *'command line tools'*, this can be done from **Xcode** preferences window.

### Q: For Mac OS X users. If you stuck with **npm ERR!** while installing any module from the above, you should consider to add *'sudo'* at the beginning of your command

    sudo gem install compass

    sudo npm install -g node-gyp

    sudo npm install -g grunt-cli

